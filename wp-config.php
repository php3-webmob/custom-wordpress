<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress_test');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+0 hDeF5EDF2HPE^WNn.j#Dy@=8WeYKtOV!lY>@!v|Mq5vl:8/ zwYeugCq8$h7U');
define('SECURE_AUTH_KEY',  'Gf@c|m<^v<K^^#l~P1V]%RV})H9CMqWpJr2JJy>:X`t2@B+(=W7pwz;EDU@r#u9/');
define('LOGGED_IN_KEY',    'M{Z=vw+ox=::dP&SG@>^aw$01}0J|!%OD`Qa5}z] i$?rdvo+34n~w(VcoZzvu+*');
define('NONCE_KEY',        'E@$OZC^ExMjZcd3hXPU`}i*gLs5HqYXfR>kOD;BMZj-M*a}8E q2Wdt}T~-i+xPz');
define('AUTH_SALT',        '_D+hJ3Ac#!{|hI$Wg)-u-h210nuWjs5J~$6a%R MP:^wv{[S,;R7{t^W5R/<B!2o');
define('SECURE_AUTH_SALT', 'SRd6y<%Z=}7 i?fcgFJhON+61a<g Ra^NH(*bmHP%cfAlFD7(55^/{bg_?/dv8HO');
define('LOGGED_IN_SALT',   'Kosv`vxTv=pQ Zk[y~w-7c;7_i&>7=E*8y9)A||*O$^H=/C*lhT,[mb,Hs)m>vRj');
define('NONCE_SALT',       '=w=g%2wfie:TA/4P.yHzi:Fzi/%Y<+wE=`%8*i)9F/aB3<w}E1D7H0P<dk}&%t/A');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
define('FS_METHOD','direct');
