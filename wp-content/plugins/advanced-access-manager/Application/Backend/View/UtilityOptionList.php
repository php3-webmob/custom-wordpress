<?php

/**
 * ======================================================================
 * LICENSE: This file is subject to the terms and conditions defined in *
 * file 'license.txt', which is part of this source code package.       *
 * ======================================================================
 */

return array(
    'manage-capability' => array(
        'title' => __('Edit/Delete Capabilities', AAM_KEY),
        'descr' => AAM_Backend_View_Helper::preparePhrase('[Please note!] For experienced users only. Allow to edit or delete capabilities', 'b'),
        'value' => AAM_Core_Config::get('manage-capability', false)
    ),
    'backend-access-control' => array(
        'title' => __('Backend Access Control', AAM_KEY),
        'descr' => __('Allow AAM to manage access to backend resources like backend menu, categories or posts.', AAM_KEY),
        'value' => AAM_Core_Config::get('backend-access-control', true),
    ),
    'frontend-access-control' => array(
        'title' => __('Frontend Access Control', AAM_KEY),
        'descr' => __('Allow AAM to manage access to frontend resources like pages, categories or posts.', AAM_KEY),
        'value' => AAM_Core_Config::get('frontend-access-control', true),
    ),
    'media-access-control' => array(
        'title' => __('Media Files Access Control', AAM_KEY),
        'descr' => AAM_Backend_View_Helper::preparePhrase('Allow AAM to manage a physically access to all media files located in the [uploads] folder.', 'strong'),
        'value' => AAM_Core_Config::get('media-access-control', false),
    )
);