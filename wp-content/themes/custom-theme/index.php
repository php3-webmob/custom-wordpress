<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Custom Wordpress</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>/wp-content/themes/custom-theme/style.css" rel="stylesheet">
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

</head>
<body>

<!----      Header Custom    ---->

<?php if ( is_home() && ! is_front_page() ) : ?>

<header class="page-header">

  <h1 class="page-title"><?php single_post_title(); ?></h1>

</header>

<?php else : ?>

<?php get_header('header'); ?>

<?php endif; ?>

<!----      Header Custom    ---->

<!----      Body Custom    ---->

<?php   get_template_part('dashboard');     ?>
<!----      Body Custom    ---->

<!----      Footer Custom    ---->

<footer class="blog-footer">
    <?php   get_footer('footer');    ?>
</footer>

<!----      Footer Custom    ---->

</body>
</html>